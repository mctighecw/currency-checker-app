# README

This is my first React Native project. It is a small app that fetches currency exchange rates from the web.

## App Information

App Name: currency-checker-app

Created: Fall 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/currency-checker-app)

## Notes

* React Native
* Expo

_Note_: It is useful to install [Watchman](https://facebook.github.io/watchman/docs/install.html) for development.

## Credits

Icons are used with permission from [FlatIcon](https://www.flaticon.com)

Last updated: 2024-10-26
