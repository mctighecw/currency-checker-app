import { StyleSheet } from 'react-native';

const currencyFields = StyleSheet.create({
  container: {
    width: '90%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    backgroundColor: '#ccc',
    borderRadius: 4,
    padding: 20,
  },
  label: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 8,
  },
  currencyTag: {
    fontSize: 14,
    fontWeight: '500',
  },
  fields: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  currencyInput: {
    width: 100,
    height: 40,
    borderRadius: 4,
    backgroundColor: '#fff',
    marginRight: 10,
  },
  space: {
    height: 30,
  },
});

export default currencyFields;
