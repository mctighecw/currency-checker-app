import { StyleSheet } from 'react-native';

const main = StyleSheet.create({
  container: {
    height: '100%',
  },
  content: {
    flex: 1,
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 60,
  },
  heading: {
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
    paddingBottom: 20,
  },
  description: {
    fontSize: 16,
    fontStyle: 'italic',
    textAlign: 'center',
    marginBottom: 10,
  },
  date: {
    fontSize: 12,
    fontStyle: 'italic',
    textAlign: 'center',
    paddingBottom: 20,
  },
});

export default main;
