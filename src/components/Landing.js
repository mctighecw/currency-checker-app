import React from 'react';
import { View, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { Button } from 'react-native-elements'
import Svg from 'react-native-svg-uri';
import currencyGraph from '../assets/graph.svg';
import styles from '../styles/landing';

class Landing extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.heading}>
            Currency Checker
          </Text>

          <View style={styles.iconContainer}>
            <Svg
              width="50"
              height="50"
              source={currencyGraph}
            />
          </View>

          <Text style={styles.description}>
            This app fetches live data about currency exchange rates from the web.
            You will be able to compare currency values with each other, in real time.
          </Text>

          <View style={styles.buttonContainer}>
            <Button
              raised
              title='Continue'
              accessibilityLabel="continue to app"
              buttonStyle={styles.button}
              onPress={() => Actions.main()}
            />
          </View>
        </View>
      </View>
    );
  }
}

export default Landing;
