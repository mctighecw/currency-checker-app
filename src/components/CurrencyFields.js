import React from 'react';
import { View, Text, TextInput } from 'react-native';
import styles from '../styles/currencyFields';

class CurrencyFields extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      currencyTypeFrom: 'usd',
      currencyTypeTo: 'eur',
      currencyValueFrom: '',
      currencyValueTo: '',
    };
  }

  render() {
    const {
      currencyTypeFrom,
      currencyTypeTo,
      currencyValueFrom,
      currencyValueTo
    } = this.state;

    return (
      <View style={styles.container}>

        <Text style={styles.label}>From</Text>
        <View style={styles.fields}>
          <TextInput
             keyboardType='numeric'
             onChangeText={(text) => this.setState({ currencyValueFrom: text })}
             value={currencyValueFrom}
             maxLength={6}
             style={styles.currencyInput}
          />
          <Text style={styles.currencyTag}>USD</Text>
        </View>

        <View style={styles.space} />

        <Text style={styles.label}>To</Text>
        <View style={styles.fields}>
          <TextInput
             keyboardType='numeric'
             onChangeText={(text) => this.setState({ currencyValueTo: text })}
             value={currencyValueTo}
             maxLength={6}
             style={styles.currencyInput}
          />
          <Text style={styles.currencyTag}>EUR</Text>
        </View>

      </View>
    );
  }
}

export default CurrencyFields;
