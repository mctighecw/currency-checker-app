import React from 'react';
import { View, Text } from 'react-native';
import { getCurrentDateTime } from '../utils/functions';
import CurrencyFields from './CurrencyFields';
import styles from '../styles/main';

class Main extends React.Component {
  render() {
    const today = getCurrentDateTime();

    return (
      <View style={styles.container}>
        <View style={styles.content}>
          <Text style={styles.heading}>
            Exchange Rates
          </Text>

          <Text style={styles.description}>
            Choose your currencies below
          </Text>

          <CurrencyFields />
        </View>

        <Text style={styles.date}>{today}</Text>
      </View>
    );
  }
}

export default Main;
