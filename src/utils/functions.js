export const getCurrentDateTime = () => {
  const date = new Date();

  const options = {
    weekday: 'short',
    month: 'short',
    day: 'numeric',
    year: 'numeric',
    hour: '2-digit',
    minute: '2-digit'
  };

  return date.toLocaleTimeString('en-us', options);
};
