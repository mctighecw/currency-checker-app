import React from 'react';
import { Router, Stack, Scene } from 'react-native-router-flux';

import Landing from './src/components/Landing';
import Main from './src/components/Main';

class App extends React.Component {
  render() {
    return (
      <Router>
        <Stack key="root">
          <Scene key="landing" component={Landing} />
          <Scene key="main" component={Main} />
        </Stack>
      </Router>
    );
  }
}

export default App;
